CREATE TABLE trabajador(
	cedula VARCHAR(15) NOT NULL,
	nombre VARCHAR(64) NOT NULL,
	apellido VARCHAR(64) NOT NULL,
	direccion VARCHAR(256) NOT NULL,
	email VARCHAR(64) NOT NULL,
	cargo VARCHAR(64) NOT NULL,
	fijo BOOLEAN,
	PRIMARY KEY (cedula)
);

CREATE TABLE persona(
	cedula VARCHAR(15) NOT NULL,
	nombre VARCHAR(64) NOT NULL,
	apellido VARCHAR(64) NOT NULL,
	direccion VARCHAR(256) NOT NULL,
	email VARCHAR(64) NOT NULL,
	ocupacion VARCHAR(64) NOT NULL,
	PRIMARY KEY (cedula)
);

CREATE TABLE area(
	id_area VARCHAR(64) NOT NULL,
	nombre_area VARCHAR(64),
	descripcion VARCHAR(256),
	sala BOOLEAN,
	estado VARCHAR(64),
	capacidad INT,
	PRIMARY KEY(id_area)
);

CREATE TABLE articulo(
	id_articulo VARCHAR(10) NOT NULL,
	nombre_articulo VARCHAR(64) NOT NULL,
	descripcion VARCHAR(256) NOT NULL,
	cantidad INT NOT NULL,
	num_bien VARCHAR(10) NOT NULL,
	estado VARCHAR(64) NOT NULL,
	dispositivo BOOLEAN,
	serial VARCHAR(64),
	marca VARCHAR(64),
	modelo VARCHAR(64),
	id_area VARCHAR(10),
	PRIMARY KEY(id_articulo)
);

CREATE TABLE solicita(
	ced_persona VARCHAR(15),
	id_articulo_solicitado VARCHAR(10),
	FOREIGN KEY(ced_persona) REFERENCES persona(cedula),
	FOREIGN KEY(id_articulo_solicitado) REFERENCES articulo(id_articulo)
);

CREATE TABLE peticion(
	cedula VARCHAR(15) NOT NULL,
	nombre VARCHAR(64) NOT NULL,
	apellido VARCHAR(64) NOT NULL,
	movil VARCHAR(64) NOT NULL,
	direccion VARCHAR(256) NOT NULL,
	PRIMARY KEY(cedula)
);

CREATE TABLE reserva(
	id_reserva SERIAL,
	ced_peticion VARCHAR(15),
	id_sala VARCHAR(10) NOT NULL,
	fecha_reserva DATE,
	hora_inicio TIMESTAMP WITH TIME ZONE,
	hora_fin TIMESTAMP WITH TIME ZONE,
	PRIMARY KEY(id_reserva),
	FOREIGN KEY(ced_peticion) REFERENCES peticion(cedula),
	FOREIGN KEY(id_sala) REFERENCES area(id_area)
	
);

INSERT INTO articulo(
            id_articulo, nombre_articulo, descripcion, cantidad, num_bien, 
            estado, dispositivo, serial, marca, modelo, id_area)
    VALUES (0000000001, 'monitor', 'monitor de pc', 5, '','nuevo', true, 1584546, 'benq', 'estandar', ''),
           (0000000002, 'impresora', 'impresora de toner', 2, '', 'usada', true, 15686, 'hp', 'mj2564', ''),
		   (0000000003, 'impresora', 'impresora de tinta continua', 3, '', 'usada', true, 1568826, 'hp', 'mj2564', ''),
		   (0000000004, 'impresora', 'impresora de tinta continua', 2, '', 'nueva', true, 156556, 'samsung', 'm2020', ''),
           (0000000005, 'lapiz', 'caja de lapiz', 15, '', 'nuevo', false, '', '', '', ''),
		   (0000000006, 'boligrafos', 'caja de boligrafos', 20, '', 'nuevo', false, '', '', '', ''),
		   (0000000007, 'rema de papel', 'hojas blancas', 15, '', 'nuevo', false, '', '', '', '');

INSERT INTO public.trabajador(
            cedula, nombre, apellido, direccion, email, cargo, fijo)
    VALUES (12345678, 'trabajador 1', 'apellido 1', 'porlamar', 'trabajador1@hotmail.com', 'obrero', true),
		   (56842359, 'trabajador 2', 'apellido 2', 'la asuncion', 'trabajador2@hotmail.com', 'algun cargo que no se...', true),
		   (12325878, 'trabajador 3', 'apellido 3', 'porlamar', 'trabajador3@hotmail.com', 'obrero', false);

INSERT INTO public.persona(
            cedula, nombre, apellido, direccion, email, ocupacion)
    VALUES (21324937, 'gregory', 'mujica', 'el salado', 'gregorymujica1993@gmail.com', 'estudiante'),
		   (27125605, 'agatha', 'pulgar', 'la fuente', 'agM@gmail.com', 'estudiante'),
		   (20123456, 'ali', 'marcano', 'santa ana', 'alM@gmail.com', 'Investigador');

INSERT INTO public.area(
            id_area, nombre_area, descripcion, sala, estado, capacidad)
    VALUES (0001, 'area1', 'esta es el area 1', true, 'Bueno', 25),
		   (0002, 'area2', 'esta es el area ', false, '', '20');




CREATE OR REPLACE FUNCTION GetAllarticulo()
RETURNS setof articulo AS
$BODY$
BEGIN 
RETURN QUERY
select * from articulo;
END;
$BODY$
LANGUAGE plpgsql;

